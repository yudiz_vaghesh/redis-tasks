const express = require('express');
const rSetcontroller = require('../controllers/rSetscontroller');
var router = express.Router();

router.get('/',rSetcontroller.test);

router.post('/sadd',rSetcontroller.saddPost);
router.get('/scard',rSetcontroller.scardGet);
router.get('/sdiff',rSetcontroller.sdiffGet);
router.get('/sdiffstore',rSetcontroller.sdiffstoreGet);
router.get('/sinter',rSetcontroller.sinterGet);
router.get('/sismember',rSetcontroller.sismemberGet);
router.get('/smembers',rSetcontroller.smembersGet);
router.put('/smove',rSetcontroller.smovePut);
router.get('/sunion',rSetcontroller.sunionGet);

module.exports = router;

