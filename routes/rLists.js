const express = require('express');
const listController = require('../controllers/rListscontroller');

var router = express.Router()

router.post('/lpush',listController.lpushPost);
router.get('/lindex',listController.lindexGet);
router.post('/linsert',listController.linsertPost);
router.get('/llen',listController.llenGet);
router.get('/lrange',listController.lrangeGet);
router.get('/lpos',listController.lposGet);

module.exports = router;