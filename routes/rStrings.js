const express = require('express');
const stringController = require('../controllers/rStringcontroller');

var router = express.Router();

router.get('/set',stringController.setItem);

module.exports = router;