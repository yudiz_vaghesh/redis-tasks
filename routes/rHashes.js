const express = require('express');
const hashController = require('../controllers/eHashcontroller');
var router = express.Router();

router.post('/hset',hashController.hsetPost);
router.get('/hget',hashController.hgetAllGet);
router.put('/hincr',hashController.hincrbyPut);
module.exports = router;