const express = require('express');
const bodyParser = require('body-parser');

// routes
const rSets = require('./routes/rSets');
const rLists = require('./routes/rLists');
const rHashes = require('./routes/rHashes');
const rStrings = require('./routes/rStrings');
const rPublish  = require('./routes/rPubsub');
// middlewares
const redis = require('./middlewares/redisConn');


var app = express();

// use the bodyParser for form or post data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extendedL:false}));

// app routes
app.use('/set',rSets);
app.use('/list',rLists);
app.use('/hash',rHashes);
app.use('/string',rStrings);
app.use('/v1',rPublish);
//run localhost server
app.listen(5001,()=>{
    console.log('Localhost connected on 5001...');
});

// redis connection
redis.connect(()=>{
    console.log('Connected with Redis...');
});

