const redis = require('../middlewares/redisConn');

const test = (req,res)=>{
    res.send("Hello Redis from controller.")
};

const saddPost = async(req,res)=>{
    // it use to add one or more values in set
    try{

        let users = await redis.sadd(req.body.key,req.body.values,(err,resp)=>{
             console.log(resp);
         }); // please use array of values in postman for body.values field
         res.send(`Sucessfully Added ${req.body.key} : ${users}`);
    }catch(error){
        console.log(error);
    }
}

const scardGet = async(req,res)=>{
    // it gives total items count in set means provide cardinality
    try{
        let totalUsers = await redis.scard(req.body.keyName);
        res.send(`Total Users : ${totalUsers}`);
    }catch(error){
        console.log(error);
    }
}

const sdiffGet = async(req,res)=>{
    // it gives different values from two or more sets
    try{
        let justUsers = await redis.sdiff(req.body.sets);

        res.send(`Users only : ${justUsers}`);
        // res.send(justUsers);
    }catch(error){
        console.log(error);
    }
}

const sdiffstoreGet = async(req,res)=>{
    // it gives different values from two or more sets like SDIFF but it store result in new set
    // if destination set is exists it will override it 
    try{
        let superUsers = await redis.sdiffstore(req.body.key,req.body.sets);

        res.send(`Superusers only : ${superUsers}`);
       
    }catch(error){
        console.log(error);
    }
}

const sinterGet = async(req,res)=>{
    // Returns the members of the set resulting from the intersection of all the given sets
    try{
        let adminUser = await redis.sinter(req.body.sets);

        res.send(`User and Admin both access : ${adminUser}`);
       
    }catch(error){
        console.log(error);
    }

    // NOTE: SINTERSTORE => This command is equal to SINTER, but instead of returning the resulting set, it is stored in destination.
                      // => If destination already exists, it is overwritten.
}

const sismemberGet = async(req,res)=>{
    // Returns if member is a member of the set stored at key.
    try{
        let isUser = await redis.sismember(req.body.key,req.body.member);
        if(isUser) return res.status(200).send(`${req.body.member} is member of ${req.body.key} set`);

        res.send(`${req.body.member} is not member of ${req.body.key} set`)
        
    }catch(error){
        console.log(error);
    }
}

const smembersGet = async(req,res)=>{
    // Returns all the members of the set value stored at key.
    // This has the same effect as running SINTER with one argument key.
    try{
        let allMembers = await redis.smembers(req.body.key);
        if(allMembers) return res.status(200).send(`[ ${allMembers} ] are members of ${req.body.key} set`);

        res.send(`${req.body.key}: Members are not found`)
        
    }catch(error){
        console.log(error);
    }
}

const smovePut = async(req,res)=>{
    // Move member from the set at source to the set at destination.
    
    try{
        let moveMember = await redis.smove(req.body.source,req.body.dest,req.body.member);
        if(moveMember) return res.status(200).send(`${req.body.member} is moved from ${req.body.source} to ${req.body.dest}`);

        res.send(`Member is not moved or Already in destination set`)
        
    }catch(error){
        console.log(error);
    }
}

const sunionGet = async(req,res)=>{
    try{
        let allMembers = await redis.sunion(req.body.sets);
        if(!allMembers) return res.status(400).send('Something wrong !!!');

        res.send(`All Members : ${allMembers}`);
    }catch(error){
        console.log(error);
    }
    // SUNIONSTORE : This command is equal to SUNION, but instead of returning the resulting set, it is stored in destination.
}
// SPOP : Removes and returns one or more random members from the set value store at key.
// SRANDMEMBER : When called with just the key argument, return a random element from the set value stored at key.
// SREM : Remove the specified members from the set stored at key. 

module.exports = { 
    test, saddPost, scardGet, sdiffGet, sdiffstoreGet,
    sinterGet, sismemberGet, smembersGet, smovePut, sunionGet
};