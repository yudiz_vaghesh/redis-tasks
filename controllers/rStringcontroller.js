const redis = require('../middlewares/redisConn');

const setItem = async (req,res)=>{

    // await redis.set('School','Ahemdabad Public School','TTL',10000);
    // await redis.mset('maths',76,'Chemistry',80)
    // await redis.msetnx('physics',76)
    // await redis.incrby('maths',-6)


    res.send('Done!!!')
}

module.exports ={
    setItem
}

// Basic Commands
// APPEND : If key already exists and is a string, this command appends the value at the end of the string.
// DECR / DECRBY : Decrement number stored at key by one and decrement specified value respectively
// GETDEL : get the key and delete that key
// GETEX : get the key with expiration option
// GETRANGE : get the subset string from string on based range
// GETSET : Atomically sets key to value and returns the old value stored at key. Returns an error when key exists but does not hold a string value.
// MGET : Returns the values of all specified keys. 
// SETEX : Set key to hold the string value and set key to timeout after a given number of seconds.
// STRALGO : Remain to learn
// STRLEN : Returns the length of the string value stored at key. An error is returned when key holds a non-string value.
