const { llen } = require('../middlewares/redisConn');
const redis = require('../middlewares/redisConn');


const lpushPost = async(req,res)=>{
    try{
        let pushData = await redis.lpush(req.body.key,req.body.data);
        if(!pushData) return res.status(400).send('Data is not added in list.')

        res.send('Success');
    }catch(error){
        console.log(error);
    }
}
const lindexGet = async(req,res)=>{
    try{
        let uIndex = await redis.lindex(req.body.key,req.body.index);
        if(!uIndex) return res.status(404).send('Data not found !!!');

        res.send(`In ${req.body.key} list at position ${req.body.index} : ${uIndex}`)
    }catch(error){
        console.log(error);
    }
}
const linsertPost = async(req,res)=>{
    try{
        var {key,direction,pivot,value} = req.body;
        let uInsert = await redis.linsert(key,direction,pivot,value);
        if(!uInsert) return res.status(404).send('Not Inserted!!!');

        res.send(`${value} is inserted ${direction} ${pivot}`)
    }catch(error){
        console.log(error);
    }
}

const llenGet = async(req,res)=>{
    try{
        
        let length = await redis.llen(req.body.key);
        if(!length) return res.status(404).send('Not Found!!!');

        res.send(`${req.body.key} list having ${length} items`)
    }catch(error){
        console.log(error);
    }
}

const lrangeGet = async(req,res)=>{
    try{
        
        let lrangeEmp = await redis.lrange(req.body.key,0,-1);
        if(!lrangeEmp) return res.status(404).send('Not Found!!!');

        res.send(`${lrangeEmp}`)
    }catch(error){
        console.log(error);
    }
}

const lposGet = async(req,res)=>{
    try{
        // The command returns the index of matching elements inside a Redis list.
        // we can found the index position of duplicate value and also count. 
        // Doubt: Why not showing index of first value.
        let matched = await redis.lpos(req.body.key,req.body.value);
        if(!matched) return res.status(404).send('Not Found!!!');

        res.send(`${req.body.value} is at index position ${matched}`)
    }catch(error){
        console.log(error);
    }
}

// Remain commands
// LMOVE : Automatically remove first/last element of the list from source and store at first/last in destination list
// LPOP : removes and return the first element from list (default), we can remove multiple elements
// LPUSHX : it insert value at head of list if list exists.
// LREM : Removes the first count occurrences of elements equal to element from the list stored at key. 
// LSET : it will set the value at specified index
// LTRIM : Trim an existing list so that it will contain only the specified range of elements specified.
// RPOP : it will remove last elment from list
// RPOPLPUSH : it will remove last element from source and store it at destination

module.exports = { lpushPost,lindexGet,linsertPost,llenGet,lrangeGet,lposGet}