const redis = require('../middlewares/redisConn');

// const channel1 = async(req,res)=>{
//     try{
//         await redis.publish('channel1',req.body.message);
//         // if(!message) return res.status(500).send('Server error...');
//         res.send('Message Published.')
//     }catch(error){
//         console.log(error);
//     }
// }
const ch1Subscribe = async(req,res)=>{
    try{
        await redis.subscribe('channel1');

        await redis.on('message',(channel,msg)=>{
            console.log(msg);
        });
    }catch(err){
        console.log(err);
    }
}
module.exports = { ch1Subscribe };

// Basic Commands
// PSUBSCRIBE : Subscribes the client to the given patterns.
// PUBLISH : post a message to given channel
// PUBSUB : The PUBSUB command is an introspection command that allows to inspect the state of the Pub/Sub subsystem.
// PUNSUBSCRIBE / UNSUBSCRIBE : Unsubscribes the client from the given patterns, or from all of them if none is given.
