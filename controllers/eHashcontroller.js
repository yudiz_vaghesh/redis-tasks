
const redis = require('../middlewares/redisConn');

const hsetPost = async(req,res)=>{
    try{
    //    await redis.hset("Student","fname","vaghesh");
    //    await redis.hmset("Student","fname","vaghesh","lname","Patel");
       let success = await redis.hsetnx("Student","lname","Patel");
       if(!success) return res.status(400).send('Error'); 
       res.send(`Success`);
    }catch(error){
        console.log(error);
    }
}

const hgetAllGet = async(req,res)=>{
    try{
       let fields = await redis.hgetall("Student");
       if(!fields) return res.status(400).send('Error'); 
       
       res.send(`Data: ${fields.fname} ${fields.lname}`);
    }catch(error){
        console.log(error);
    }
}
 
const hincrbyPut = async(req,res)=>{
    let update = await redis.hincrby('Student','marks',5);
    if(!update) return res.status(404).send('Marks is not updated...');

    res.send('updated successfully...')
}

module.exports = {hsetPost,hgetAllGet,hincrbyPut};

// Other Basic commands
// HDEL : Removes the specified fields from the hash stored at key. 
// HEXISTS : Returns if field is an existing field in the hash stored at key.
// HKEYS : Returns all field names in hash stored at key
// HMGET : Returns the values associated with the specified fields in the hash stored at key.
// HRANDFIELD : When called with just the key argument, return a random field from the hash value stored at key.
// HSTRLEN : Returns the string length of the value associated with field in the hash stored at key. If the key or the field do not exist, 0 is returned.
// HVAL : Returns all values in the hash stored at key.
